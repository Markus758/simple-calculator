//Assignment 2
//Markus Coleman
// 
// Removed the POW function.
//

#include <iostream>

using namespace std;

//calculate addition
float Add(float num1, float num2)
{
    return num1 + num2;
}

//calculate subtraction
float Subtract(float num1, float num2)
{
    return num1 - num2;
}

//calculate multiplication
float Multiply(float num1, float num2)
{
    return num1 * num2;
}

//calculate division
bool Divide(float num1, float num2, float& answer)
{
    if (num2 == 0)
        return false;

    answer = num1 / num2;

    return true;
}

int main()
{
    int num1, num2;
    char op, choice;

    //infinite loop
    while (true)
    {
        //taking user input
        cout << "Enter two positive numbers: ";
        cin >> num1 >> num2;

        cout << "Enter operator to perform calculation: ";
        cin >> op;

        //performing appropriate task
        switch (op)
        {
        case '+':
            cout << "Addition: " << Add(num1, num2) << endl;
            break;


        case '-':
            cout << "Subtraction: " << Subtract(num1, num2) << endl;
            break;


        case '*':
            cout << "Multiplication: " << Multiply(num1, num2) << endl;
            break;


        case '/':
            float answer;
            if (Divide(num1, num2, answer))
                cout << "Division: " << answer << endl;
            else
                cout << "Cannot divide by zero " << endl;
            break;
        }

        cout << "do you wish to continue(y/n): ";
        cin >> choice;
        if (choice == 'n' || choice == 'N')
            break;
    }

    cout << endl;
    return 0;
}

